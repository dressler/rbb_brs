# Reusable Building Block

## 1. The name of the RBB [_*_]

Boundedly-rational satisficer (BRS)

## 2. The author(s) names [_*_] & affiliation(s)

Gunnar Dressler, Birgit Müller
Department of Ecological Modelling, Helmholtz Centre for Environmental Research - UFZ

## 3. Keywords [_*_]

natural resource use, human behavior, decision-making

## 4.	The purpose of the RBB [_*_]

The BRS represents a simple decision-making routine of a human actor that can be applied to various contexts, e.g., (natural) resource use or product choice. The actor applies *satisficing* as a simple heuristic to select an option (in this case: a patch) from a range of available alternative options that meets a specific acceptability threshold (in this case: a desired level of resources on a patch). Satisficing has been applied in several environmental and resource contexts, e.g. fisheries[^Wijermans], agriculture[^Malawska], human-wildlife interactions[^Chion] or pastoralism[^Dressler].

## 5.	Concepts

The key concept underlying the RBB is bounded rationality: when individuals take a decision, their rationality is often limited, so that they may not take an optimal but rather a satisfactory decision. These limitations include, e.g., a limited amount of information, limited cognitive abilities, or a limited amount of time available to take a decision. Under these circumstances, individuals will act as *satisficers* instead of optimizers and will take a decision that achieves some minimum (i.e., satisfactory) level of a specific variable, which may not necessarily be the maximum value. The concept of *bounded rationality* was proposed by Herbert A. Simon as an alternative to neoclassical economics that assumes a perfectly rational decision-maker that is selfish, has perfect information, stable preferences and will take the decision that yields the highest utility for the individual[^Simon]<sup>,</sup>[^Artinger].

## 6.	An overview of the RBB and its use [_*_]

### Entity [_*_]:

**What entity, or entities, are running the submodel or are involved (e.g., by providing  information)?**
- The agents represent individual human actors (e.g., resource users such as fishermen)
- The patches represent the environment that provides resources (e.g., fish)

**What state variables does the entity need to have to run this RBB?**
- Each agent has one state variable, the *harvest level **H*** that represents the current amount of harvested resources
- Each patch is characterized by one state variable, the *resource level **R*** that represents the current amount of available resources on the patch

### Context, model parameters & patterns:

What global variables (e.g., parameters characterising the environment) or data structures (e.g., a gridded spatial environment) does the use of the RBB require?
- none

Does the RBB directly affect global variables or data structures?
- No, the RBB does not modify global variables, only state variables of the agents and patches are affected.

#### What parameters does the RBB use? Preferably a table including parameter name, meaning, default value, range, and unit (if applicable).

The RBB uses two main parameters, the *satisficing_threshold* and the *vision_radius*. Both parameters influence the decision-making of the agents, specifically the `to-select` procedure.  

The *satisficing_threshold* describes the desired level of resources that the agent wants to harvest in one time step. A lower *satisficing_threshold* value represents a more conservative agent, a higher *satisficing_threshold* a more greedy agent. The higher the *satisficing_threshold*, the more closely the agents represents a maximizer.

The *vision_radius* describes the perception radius of the agents, i.e. in which radius the agent is able to perceive the state of the patches. The *vision_radius* represents the (limited) amount of available information that the agent has available. The higher the *vision_radius*, the more closely the agent represents a rational actor.

| name                  | meaning                         | units   | typical ranges |
| --------------------- | ------------------------------- | ------- | -------------- |
| satisficing_threshold | Desired level of resources      | RU      | (0,inf)        |
| vision_radius         | Radius of perception of patches | patches | (0,inf)        |

### Interface - A list of all inputs and outputs of the RBB [_*_]

**Which input variables does the RBB require from an external, calling entity and in which units?**
- The *resource_level* of all patches within the agent's *vision_radius* (*Note: in the NetLogo implementation, resource_level is a patch variable that can be directly accessed by the procedure, and is therefore not supplied as a function argument*)

**What specific outputs does it produce and how does this update the state variables of the calling entity?**
- The RBB returns a *patch*, i.e. a location that the agent will be moving to. By moving to that patch, the agents location (i.e., x and y coordinates) will be updated.

### Scales [_*_]:

The RBB is not limited to a specific spatial or temporal scale but can be defined depending on the application context of the model. E.g., if the context is a fishermen taking a decision on where to fish, the spatial scale could represent a lake (extent) where each patch is a fishing spot (resolution) and the temporal scale a year (extent) where each time step is a fishing day (resolution).

## 7.	Pseudocode, a Flowchart or other type of graphical representation [_*_]

![](figs/RBB_FlowChart.png){width=600px}

## 8.	The program code [_*_]

### NetLogo implementation

The building block is implemented in NetLogo 6.3. The following code block shows a complete example model, the RBB is implemented in the `select-patch` procedure.

```
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Example model that shows the usage of the
;; Boundedly-rational satisficer (BRS) reusable building block
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

patches-own [
  resource_level                      ;; current level of resources on a patch
]

turtles-own [
  harvest_level                       ;; current resource harvest of an agent
]

;; MODEL SETUP ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to setup
  clear-all
  ask patches [
    ;; set the initial resource_level on all patches
    set resource_level resource_capacity * resource_init_level / 100
  ]
  create-turtles number_agents [
    ;; move to a random patch
    move-to one-of patches
  ]
  reset-ticks
end

;; GO PROCEDURE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to go
  ask patches
  [
    ;; update the resource level
    resource-grow
    ;; update the patch color to reflect the level of resources
    set pcolor scale-color green resource_level (resource_capacity + 10) -10
  ]
  ask turtles [
    ;; agents select a new resource patch using the BRS
    let new_patch select-patch
    move-to new_patch
    ;; agents harbes the available resources
    harvest-resources
  ]
  tick
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Boundedly-rational satisficer (BRS) building block
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to-report select-patch
  ;; initially, make the current patch the selected patch
  let selected_patch patch-here
  ;; check resource level of current patch and compare to satisficing threshold
  if resource_level < satisficing_threshold [
    ;; current patch does not provide enough resources to reach the satisficing
    ;; threshold of the agent --> select new patch
    ;; consider only patches within agent's vision radius -> bounded information
    let relevant_patches patches in-radius vision_radius
    ;; check if any satisfying patches are available
    ifelse any? relevant_patches with [ resource_level > satisficing_threshold ] [
      ;; yes - a satisfying patch is available
      ;; choose a patch randomly out of the set of satisfying patches
      set selected_patch one-of relevant_patches with
        [ resource_level > satisficing_threshold ]
    ] [
      ;; no - there aren't any satisfying patches available in the vision radius
      ;; choose a patch randomly
      set selected_patch one-of relevant_patches
    ]
  ]
  ;; report the selected patch
  report selected_patch
end

;; FURTHER MODEL PROCEDURES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to resource-grow
  ;; increase resource_level according to logistic growth function
  set resource_level resource_level + resource_growth_rate * resource_level *
    (1 - resource_level / resource_capacity)
end

to harvest-resources
  ;; harvest resources on the current patch
  ;; harvest = minimum of available resource and the satisficing_threshold of
  ;; the agents (= desired harvest level)
  let available_resources resource_level - resource_harvest_limit
  set harvest_level min list available_resources satisficing_threshold
  ;; adjust the resource level on the patch by subtracting the current harvest_level
  set resource_level resource_level - harvest_level
end

to-report resource_level_mean
  report mean [ resource_level ] of patches
end

to-report harvest_level_mean
  report mean [ harvest_level ] of turtles
end
```

## 9.	Example analyses of a simulation output, test cases and benchmarks [_*_]

One sample analysis that can be conducted with the RBB is to analyze the influence of the *satisficing threshold* and the *vision radius* on the average resource level (i.e., the state of the environment) as well as the average harvest level (i.e., the state of the agents). The *satisficing threshold* determines which level of resources agents aspire to get, and the *vision radius* determines the radius in which agents perceive and select patches. Dependent on the *vision radius* of the agents and the *initial resource level* within the simulation, qualitatively different dynamics can be observed that resemble a tipping-point behavior. 

![](figs/RBB_ExampleAnalysis_ResourceLevel.png){width=600px}<br>
**Fig. 1:** Influence of *satisficing threshold* on the *resource level* for four selected levels of *vision radius* and two *initial resource levels*. A higher *satisficing threshold* leads to a lower level of resources in all cases. In the case of *vision radius = 1*, the resource level continously declines, whereas for *vision radius > 1* a sharp transition can be observed. 

![](figs/RBB_ExampleAnalysis_HarvestLevel.png){width=600px}<br>
**Fig. 2:** Influence of *satisficing threshold* on the agent's *harvest level* for four selected levels of *vision radius* and two *initial resource levels*. An increase of the *satisficing threshold* leads to a linear increase of the *harvest level* up to a certain point where agent's are not able to reach their aspired level of resources. An increase of the *vision radius* enables agents to realize a higher *satisficing threshold*. 

## 10. Version control [_*_]

- Publish Date: 2023-11-06
- Last Updated: 2024-02-05

## 11. Status info

- Peer Review: no
- Licence: GPL 3.0

## 12. Citation of the RBB

Dressler, G., Müller, B. (2024). Reusable building block of a Boundedly-rational satisficer (BRS). 

## 13. Example implementation of the RBB to demonstrate its use

An example implementation of the RBB in NetLogo is included in the repository in the `netlogo_rbb` folder.

## 14. References

[^Artinger]: Artinger, F. M., Gigerenzer, G., & Jacobs, P. (2022). Satisficing: Integrating Two Traditions. Journal of Economic Literature, 60(2), 598–635. https://doi.org/10.1257/jel.20201396
[^Chion]: Chion, C., Lamontagne, P., Turgeon, S., Parrott, L., Landry, J.-A., Marceau, D. J., Martins, C. C. A., Michaud, R., Ménard, N., Cantin, G., & Dionne, S. (2011). Eliciting cognitive processes underlying patterns of human–wildlife interactions for agent-based modelling. Ecological Modelling, 222(14), 2213–2226. https://doi.org/10.1016/j.
[^Dressler]: Dressler, G., Groeneveld, J., Buchmann, C. M., Guo, C., Hase, N., Thober, J., Frank, K., & Müller, B. (2019). Implications of behavioral change for the resilience of pastoral systems—Lessons from an agent-based model. Ecological Complexity, 40. https://doi.org/10.1016/j.ecocom.2018.06.002ecolmodel.2011.02.014
[^Malawska]: Malawska, A., & Topping, C. J. (2016). Evaluating the role of behavioral factors and practical constraints in the performance of an agent-based model of farmer decision making. Agricultural Systems, 143, 136–146. https://doi.org/10.1016/j.agsy.2015.12.014
[^Simon]: Simon, H. A. (1955). A Behavioral Model of Rational Choice. The Quarterly Journal of Economics, 69(1), 99. https://doi.org/10.2307/1884852
[^Wijermans]: Wijermans, N., Boonstra, W. J., Orach, K., Hentati‐Sundberg, J., & Schlüter, M. (2020). Behavioural diversity in fishing—Towards a next generation of fishery models. Fish and Fisheries, 21(5), 872–890. https://doi.org/10.1111/faf.12466
